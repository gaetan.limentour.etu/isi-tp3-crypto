##
# Color  Variables
##
red='\e[31;4m'
green='\e[32m'
blue='\e[34m'
clear='\e[0m'

garbage(){
	num=
	nom=
}


AddBankCard(){
		echo "-- Ajouter une carte bleue -- "
		while ! [[ "$nom" =~ ^([a-zA-Z]+[[:space:]])+$ ]]
		do
			read -p "Veuillez saisir le nom de la carte a ajouter " nom
		done
		while ! [[ "$num" =~ ^[0-9]+$ ]] 
		do
         	read -p "Veuillez saisir le numero de la carte a ajouter " num
		done
		./scripts/addBankCard.sh "$nom" "$num"
		garbage
		echo
}

DeleteBankCard(){
		echo "-- Supprimer une carte bleue -- "
		while [[ -z "$nom" ]]
		do
			read -p "Veuillez saisir le nom de la carte a supprimer " nom
		done
		while ! [[ "$num" =~ ^[0-9]+$ ]] 
		do
         	read -p "Veuillez saisir le numero de la carte a supprimer " num
		done
		./scripts/deleteBankCard.sh "$nom" "$num"
		garbage
		echo
}

SearchBankCard(){
		echo "-- Chercher une carte bleue -- "
		while [[ -z "$nom" ]]
		do
			read -p "Veuillez saisir le nom de la carte a rechercher " nom
			 
		done
		./scripts/searchBankCard.sh "$nom"
		garbage
		echo
}

WrongCommand(){
	echo "option inconnue"
	echo
}

menu(){
echo -ne $red"-- Bank Card Menu --$clear
$green
1$clear) Ajouter une carte bleue$green
2$clear) Supprimer une carte bleue$green
3$clear) Rechercher une carte bleue$green
0$clear) Quitter$blue

Choisissez une option:$clear
"
    read a
        case $a in
	        1) AddBankCard ; menu ;;
	        2) DeleteBankCard ; menu ;;
	        3) SearchBankCard ; menu ;;
			0) exit 0 ;;
			*) WrongCommand; menu;;
        esac
}

clear
menu