if cat RAMDisk/codes.txt | grep '^'"$1"':' >/dev/null 2>&1
then
	echo -n "numero de carte de $1: " 
	cat RAMDisk/codes.txt | grep '^'"$1"':' | awk -F : '{print $2}'
else
	echo "pas de carte a ce nom"
fi