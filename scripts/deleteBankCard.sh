if ! grep '^'"$1"':'"$2"'$' RAMDisk/codes.txt >/dev/null 2>&1
then
	echo "La carte n'existe pas"
	exit 1
fi

sed -i".bak" '/^'"$1"':'"$2"'$/d' RAMDisk/codes.txt
rm RAMDisk/codes.txt.bak
if ! grep '^'"$1"':'"$2"'$' RAMDisk/codes.txt >/dev/null 2>&1
then
	echo "Carte supprimé avec succes"
	openssl enc -aes-256-cbc -pbkdf2 -k azertyuiopqsdfghjklmwxcvbn -in RAMDisk/codes.txt -out HardDrive/codes.enc
else
	echo "Erreur Carte non supprimé"
fi