#Initialise toutes les clés USB

rm -r USB1
rm -r USB2
rm -r USBSUP1
rm -r USBSUP2

mkdir USB1
mkdir USB2
mkdir USBSUP1
mkdir USBSUP2

echo bisbis >> USB1/cle1.txt
echo bisbisSup >> USBSUP1/cle1Sup.txt

echo tchoupa >> USB2/cle2.txt
echo tchoupaSup >> USBSUP2/cle2Sup.txt

openssl enc -aes-256-cbc -pbkdf2 -k password1 -in USB1/cle1.txt -out USB1/cle1.enc
openssl enc -aes-256-cbc -pbkdf2 -k password2 -in USB2/cle2.txt -out USB2/cle2.enc
openssl enc -aes-256-cbc -pbkdf2 -k password1SUP -in USBSUP1/cle1Sup.txt -out USBSUP1/cle1Sup.enc
openssl enc -aes-256-cbc -pbkdf2 -k password2SUP -in USBSUP2/cle2Sup.txt -out USBSUP2/cle2Sup.enc

rm USB1/cle1.txt
rm USB2/cle2.txt
rm USBSUP1/cle1Sup.txt
rm USBSUP2/cle2Sup.txt
