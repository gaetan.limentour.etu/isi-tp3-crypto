rm -r HardDrive
rm -r RAMDisk

mkdir HardDrive
mkdir RAMDisk

./scripts/generateBankCard.sh

echo azertyuiopqsdfghjklmwxcvbn >> RAMDisk/keyMaster.txt

# key master avec 2 responsables
openssl enc -aes-256-cbc -pbkdf2 -k bisbis -in RAMDisk/keyMaster.txt -out HardDrive/tmp.enc
openssl enc -aes-256-cbc -pbkdf2 -k tchoupa -in HardDrive/tmp.enc -out HardDrive/keyMaster.enc

# key master avec responsable1 et representant2
openssl enc -aes-256-cbc -pbkdf2 -k bisbis -in RAMDisk/keyMaster.txt -out HardDrive/tmp1.enc
openssl enc -aes-256-cbc -pbkdf2 -k tchoupaSup -in HardDrive/tmp1.enc -out HardDrive/keyMaster1.enc

# key master avec responsable2 et representant1
openssl enc -aes-256-cbc -pbkdf2 -k bisbisSup -in RAMDisk/keyMaster.txt -out HardDrive/tmp2.enc
openssl enc -aes-256-cbc -pbkdf2 -k tchoupa -in HardDrive/tmp2.enc -out HardDrive/keyMaster2.enc

# encoder cartes bleues
openssl enc -aes-256-cbc -pbkdf2 -k azertyuiopqsdfghjklmwxcvbn -in RAMDisk/codes.txt -out HardDrive/codes.enc

rm RAMDisk/keyMaster.txt
rm RAMDisk/codes.txt
rm HardDrive/tmp.enc
rm HardDrive/tmp1.enc
rm HardDrive/tmp2.enc
