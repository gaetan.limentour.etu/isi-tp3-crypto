
openssl enc -d -aes-256-cbc -pbkdf2 -k $1 -in USB1/cle1.enc -out RAMDisk/key1.txt 
openssl enc -d -aes-256-cbc -pbkdf2 -k $2 -in USBSUP2/cle2Sup.enc -out RAMDisk/key2.txt

openssl enc -d -aes-256-cbc -pbkdf2 -k $(cat RAMDisk/key2.txt) -in HardDrive/keyMaster1.enc -out HardDrive/tmp1.enc

openssl enc -d -aes-256-cbc -pbkdf2 -k $(cat RAMDisk/key1.txt) -in HardDrive/tmp1.enc -out RAMDisk/keyMaster.txt

rm HardDrive/tmp1.enc

openssl enc -d -aes-256-cbc -pbkdf2 -k $(cat RAMDisk/keyMaster.txt; rm RAMDisk/keyMaster.txt) -in HardDrive/codes.enc -out RAMDisk/codes.txt
