if ! ./scripts/generateKeyMaster.sh password1 password2  > /dev/null 2>&1
then 
	rm HardDrive/keyMaster.enc
fi

if ! ./scripts/generateKeyMasterSup1.sh password1 password2SUP  > /dev/null 2>&1
then 
	rm HardDrive/keyMaster1.enc
fi

if ! ./scripts/generateKeyMasterSup2.sh password1SUP password2 > /dev/null 2>&1
then 
	rm HardDrive/keyMaster2.enc
fi
