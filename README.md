Binome
=======

Gaëtan Limentour: gaetan.limentour.etu@univ-lille.fr

Questions
---------

**Question 1 : </br>
Proposez une solution pour que le déchiffrement ne soit rendu possible que
par l’authentification à deux facteurs des deux responsables.**

Dans un premier temps, il faut générer les deux clés (cryptées avec un mot de passe), qui seront stockées respectivement sur les deux clés usb des deux administrateurs. Les deux clés seront cryptées.

Les cryptages se feront via `aes-256-cbc`

Cela permettra de générer key_master qui va permettre d'encrypter le fichier. Celui-ci sera stocké dans temporairement dans la RAM (qui sera donc supprimé lorsque la machine sera mise hors tension).

Dans le fichier key_master, se trouvera la clé qui permet de décoder le fichier encrypté. key_master sera aussi encrypté

On aura donc un mot de passe par clé USB. Lorsque chacun des admin vont entrer leur mot de passe, les clés cryptées qui sont stockées sur ces clés usb vont être récupérées.

Cela nous donnera accès au key_master qui nous permettra de decrypter le fichier.

**Question 2 : </br>
Proposez une implémentation en bash, zsh ou en python des services 1.i., 1.ii.,
1.iii. et 1.iv.. De plus, comme la mise en service suppose que les clefs usb des responsables et éventuellement le disque dur aient été initialisés d’une certaine manière, proposez un service 1.v. initialisation qui initialise les deux clefs usb et le disk.**


**Question 3 : </br>
Proposez une évolution de votre solution afin que les representants puissent se
substituer aux responsables en cas de besoin lors de la (re)mise en service du
serveur.**

Plusieurs encryptions disponnibles dans le disque dur, et selon la personne qui est avec un certain admin (tel suppléant avec tel admin), au lieu de décoder le fichier menant a key_master, on peut accéder à un autre fichier qui mène aussi a un key_master différent.

**Question 4 : </br>
Modifiez en conséquence votre service de démarrage et les autres services si
besoin.**


**Question 5 : </br>
Proposez un nouveau service : 1.vi. répudiation qui doit permettre au serveur
de retirer la responsabilité d’un responsable ou de son représentant.**

**Question 6 : </br>
Implémentez un service 1.vi. répudation conformément à votre proposition faire
en réponse de la question 5.**